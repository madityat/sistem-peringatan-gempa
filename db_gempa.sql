-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03 Apr 2019 pada 16.25
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_gempa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_sensor`
--

CREATE TABLE `data_sensor` (
  `id` int(11) NOT NULL,
  `node_name` varchar(50) NOT NULL,
  `earthquake_strength` float NOT NULL,
  `location` varchar(15) NOT NULL COMMENT 'lokasi server',
  `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_sensor`
--

INSERT INTO `data_sensor` (`id`, `node_name`, `earthquake_strength`, `location`, `lastUpdate`) VALUES
(1, 'node-1-1', 0.3, 'perangkat-1', '2019-04-03 11:50:20'),
(2, 'node-1-2', 5.9, 'perangkat-1', '2019-04-03 11:50:20'),
(3, 'node-1-3', 3.6, 'perangkat-1', '2019-04-03 11:50:20'),
(4, 'node-1-4', 7.3, 'perangkat-1', '2019-04-03 11:50:20'),
(5, 'node-1-5', 6.2, 'perangkat-1', '2019-04-03 11:50:20'),
(6, 'node-2-1', 6.8, 'perangkat-2', '2019-04-03 11:50:20'),
(7, 'node-2-2', 6.7, 'perangkat-2', '2019-04-03 11:50:20'),
(8, 'node-2-3', 6, 'perangkat-2', '2019-04-03 11:50:20'),
(11, 'server-perangkat-1', 0, 'server-utama', '2019-04-03 11:24:38'),
(12, 'server-perangkat-2', 0, 'server-utama', '2019-03-28 12:25:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_sensor_history`
--

CREATE TABLE `data_sensor_history` (
  `id` int(11) NOT NULL,
  `node_name` varchar(15) NOT NULL,
  `earthquake_strength` float NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `proses_time`
--

CREATE TABLE `proses_time` (
  `id` int(11) NOT NULL,
  `location` varchar(15) NOT NULL COMMENT 'lokasi server',
  `data_dikirim` datetime(6) NOT NULL,
  `data_diterima` datetime(6) NOT NULL,
  `data_update_selesai` datetime(6) NOT NULL,
  `fuzzy_selesai` datetime(6) NOT NULL,
  `proses_selesai` datetime(6) NOT NULL,
  `lama_waktu` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_sensor`
--
ALTER TABLE `data_sensor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `node_name` (`node_name`);

--
-- Indexes for table `data_sensor_history`
--
ALTER TABLE `data_sensor_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proses_time`
--
ALTER TABLE `proses_time`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_sensor`
--
ALTER TABLE `data_sensor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `data_sensor_history`
--
ALTER TABLE `data_sensor_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `proses_time`
--
ALTER TABLE `proses_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
